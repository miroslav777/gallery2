package gallery.com.gallery2.presenter;


import android.graphics.Bitmap;

import gallery.com.gallery2.MyApp;
import gallery.com.gallery2.internet.repo.LoadImageRepository;
import gallery.com.gallery2.internet.repo.tasks.LoadImageListTask;
import gallery.com.gallery2.view.interfaces.ViewCell;

public class ViewCellPresenter implements LoadImageListTask.DownloadListener {

    private ViewCell viewCell;
    private LoadImageRepository loadImageRepository;

    public ViewCellPresenter(ViewCell viewCell) {
        this.viewCell = viewCell;
    }

    public void uploadPicture(String url){
        viewCell.hidePicture();
        viewCell.showLoading();
        loadImageRepository = MyApp.getLoadImageRepository();
        loadImageRepository.loadImages(url,this);
    }

    @Override
    public void onProgressUpdate(int progress) {
        viewCell.setProgress(progress);
    }

    @Override
    public void onPostExecute(Bitmap bitmap) {
        viewCell.hideLoading();
        viewCell.hideProgress();
        viewCell.showPicture(bitmap);
        loadImageRepository = null;
    }
}

