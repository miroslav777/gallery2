package gallery.com.gallery2.presenter;


import android.graphics.Bitmap;
import gallery.com.gallery2.MyApp;
import gallery.com.gallery2.internet.repo.LoadImageRepository;
import gallery.com.gallery2.internet.repo.models.Hits;
import gallery.com.gallery2.internet.repo.tasks.LoadImageListTask;
import gallery.com.gallery2.view.interfaces.DetailWindow;

public class DetailWindowPresenter implements LoadImageListTask.DownloadListener{

    private DetailWindow detailWindow;
    private static DetailWindowPresenter detailWindowPresenter;
    private LoadImageRepository loadImageRepository = MyApp.getLoadImageRepository();

    public static DetailWindowPresenter getInstance(DetailWindow detailWindow, Hits hit){
        detailWindowPresenter = new DetailWindowPresenter();
        detailWindowPresenter.setDetailWindow(detailWindow);
        detailWindowPresenter.setHits(hit);
        return detailWindowPresenter;
    }

    private void setDetailWindow(DetailWindow detailWindow) {
        this.detailWindow = detailWindow;
    }

    private void setHits(Hits hit){
        loadImageRepository.loadImages(hit.getPreviewurl(), this);
        detailWindow.hidePicture();
        detailWindow.showLoading();
    }

    @Override
    public void onProgressUpdate(int progress) {

    }

    @Override
    public void onPostExecute(Bitmap bitmap) {
        detailWindow.hideLoading();
        detailWindow.showPicture(bitmap);
    }
}
