package gallery.com.gallery2.presenter;


import java.util.List;
import java.util.concurrent.ExecutionException;

import gallery.com.gallery2.MyApp;
import gallery.com.gallery2.adapter.MainRecyclerViewAdapter;
import gallery.com.gallery2.internet.repo.WallpapersAPIRepo;
import gallery.com.gallery2.internet.repo.models.Hits;
import gallery.com.gallery2.internet.repo.tasks.LoadBodyWallpapersTask;
import gallery.com.gallery2.view.interfaces.MainWindow;

public class MainPresenter implements LoadBodyWallpapersTask.DownloadListener, MainRecyclerViewAdapter.Listener {

    private MainWindow mainWindow;
    private WallpapersAPIRepo wallpapersAPIRepo;

    public MainPresenter(MainWindow mainWindow){
        this.mainWindow = mainWindow;
        wallpapersAPIRepo = MyApp.getWallpapersAPIRepository();
        notifyAboutEmptyList();
    }

    public void notifyAboutEmptyList(){

        try {
            wallpapersAPIRepo.loadUrlList(this);

        } catch (ExecutionException e) {
            mainWindow.showErrorMessage();
        } catch (InterruptedException e) {
            mainWindow.showErrorMessage();
        }
        finally {
            mainWindow.hideErrorMessage();
        }
    }

    @Override
    public void onPostExecute(List<Hits> hitsList) {
        mainWindow.notifyAboutNewUrlList(hitsList);
        wallpapersAPIRepo.removeTask();
    }

    @Override
    public void onClick(int position) {
        mainWindow.startDetailWindow(position);
    }
}
