package gallery.com.gallery2.internet.repo;

import android.os.AsyncTask;

import java.util.HashMap;
import java.util.Map;

import gallery.com.gallery2.MyApp;
import gallery.com.gallery2.internet.repo.tasks.LoadImageListTask;


public class LoadImageRepository {

    private Map<Integer,AsyncTask> taskMap = new HashMap<>();

    public void loadImages(String url, LoadImageListTask.DownloadListener downloadListener) {

        if (isExist(downloadListener.hashCode())){
            AsyncTask asyncTask = taskMap.get(downloadListener.hashCode());
            if (!asyncTask.isCancelled()){
                asyncTask.cancel(true);
            }
            taskMap.remove(downloadListener.hashCode());
        }

        if (MyApp.getTaskManager().isExist(url)){
            taskMap.put(downloadListener.hashCode(), MyApp.getDiskCacheTask().loadImageFromDir(downloadListener,url));
            return;
        }
        taskMap.put(downloadListener.hashCode(), new LoadImageListTask(downloadListener).execute(url));
        MyApp.getTaskManager().addUrl(url);
    }

    private boolean isExist(int hashCode){
        return taskMap.containsKey(hashCode);
    }
}
