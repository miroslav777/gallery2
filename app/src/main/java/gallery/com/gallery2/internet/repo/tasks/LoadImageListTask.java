package gallery.com.gallery2.internet.repo.tasks;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import gallery.com.gallery2.MyApp;
import gallery.com.gallery2.model.Picture;

public class LoadImageListTask extends AsyncTask<String,Integer,Bitmap>{

    private DownloadListener downloadListener;

    public LoadImageListTask(DownloadListener downloadListener){
        this.downloadListener = downloadListener;
    }

    @Override
    protected Bitmap doInBackground(String... lists) {

        Bitmap bitmap = null;
        HttpURLConnection httpURLConnection = null;
        BufferedInputStream bufferedInputStream = null;

            try {
                httpURLConnection = (HttpURLConnection) new URL(lists[0]).openConnection();
                httpURLConnection.setDoInput(true);
                httpURLConnection.setRequestProperty("Connection", "Keep-Alive");
                httpURLConnection.connect();
                int contentLength = httpURLConnection.getContentLength();

                bufferedInputStream = new BufferedInputStream(httpURLConnection.getInputStream());
                /*
                int count = 0;
                int total = 0;
                byte[] totalBytes = new byte[contentLength];
                byte[] bytes = new byte[1024];

                while ((count = bufferedInputStream.read(totalBytes,total,Math.min(1000, totalBytes.length - total)))!=0){
                    total += count;
                    publishProgress(total*100/contentLength);
                }

                bitmap = BitmapFactory.decodeByteArray(totalBytes,0,totalBytes.length);
                */
                bitmap = BitmapFactory.decodeStream(bufferedInputStream);
                MyApp.getDiskCacheTask().loadImageToDir(new Picture(bitmap,lists[0]));
                bufferedInputStream.close();
                httpURLConnection.disconnect();
                httpURLConnection = null;
            } catch (IOException e) {
                e.printStackTrace();
            }

        return bitmap;
    }


    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        if (downloadListener!=null){
            downloadListener.onProgressUpdate(values[0]);
        }
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
        if (downloadListener != null){
            downloadListener.onPostExecute(bitmap);
            downloadListener = null;
        }
    }

    public interface DownloadListener{
        void onProgressUpdate(int progress);
        void onPostExecute(Bitmap bitmap);
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        if (downloadListener!=null){
            downloadListener = null;
        }
    }
}
