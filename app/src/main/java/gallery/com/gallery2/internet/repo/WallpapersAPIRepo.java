package gallery.com.gallery2.internet.repo;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;

import gallery.com.gallery2.internet.repo.tasks.LoadBodyWallpapersTask;


public class WallpapersAPIRepo {
    private LoadBodyWallpapersTask loadBodyWallpapersTask;

    public void loadUrlList(LoadBodyWallpapersTask.DownloadListener downloadListener) throws ExecutionException, InterruptedException {
       if (loadBodyWallpapersTask == null){
           loadBodyWallpapersTask = new LoadBodyWallpapersTask(downloadListener);
           loadBodyWallpapersTask.executeOnExecutor(Executors.newSingleThreadExecutor());
       }
    }

    public void removeTask(){
        if (loadBodyWallpapersTask!=null){
            loadBodyWallpapersTask = null;
        }
    }
}
