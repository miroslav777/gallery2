package gallery.com.gallery2.internet.repo.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Hits implements Serializable{
    @SerializedName("previewHeight")
    private int previewheight;
    @SerializedName("likes")
    private int likes;
    @SerializedName("favorites")
    private int favorites;
    @SerializedName("tags")
    private String tags;
    @SerializedName("webformatHeight")
    private int webformatheight;
    @SerializedName("views")
    private int views;
    @SerializedName("webformatWidth")
    private int webformatwidth;
    @SerializedName("previewWidth")
    private int previewwidth;
    @SerializedName("comments")
    private int comments;
    @SerializedName("downloads")
    private int downloads;
    @SerializedName("pageURL")
    private String pageurl;
    @SerializedName("previewURL")
    private String previewurl;
    @SerializedName("webformatURL")
    private String webformaturl;
    @SerializedName("imageWidth")
    private int imagewidth;
    @SerializedName("user_id")
    private int userId;
    @SerializedName("user")
    private String user;
    @SerializedName("type")
    private String type;
    @SerializedName("id")
    private int id;
    @SerializedName("userImageURL")
    private String userimageurl;
    @SerializedName("imageHeight")
    private int imageheight;

    public int getPreviewheight() {
        return previewheight;
    }

    public void setPreviewheight(int previewheight) {
        this.previewheight = previewheight;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public int getFavorites() {
        return favorites;
    }

    public void setFavorites(int favorites) {
        this.favorites = favorites;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public int getWebformatheight() {
        return webformatheight;
    }

    public void setWebformatheight(int webformatheight) {
        this.webformatheight = webformatheight;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }

    public int getWebformatwidth() {
        return webformatwidth;
    }

    public void setWebformatwidth(int webformatwidth) {
        this.webformatwidth = webformatwidth;
    }

    public int getPreviewwidth() {
        return previewwidth;
    }

    public void setPreviewwidth(int previewwidth) {
        this.previewwidth = previewwidth;
    }

    public int getComments() {
        return comments;
    }

    public void setComments(int comments) {
        this.comments = comments;
    }

    public int getDownloads() {
        return downloads;
    }

    public void setDownloads(int downloads) {
        this.downloads = downloads;
    }

    public String getPageurl() {
        return pageurl;
    }

    public void setPageurl(String pageurl) {
        this.pageurl = pageurl;
    }

    public String getPreviewurl() {
        return previewurl;
    }

    public void setPreviewurl(String previewurl) {
        this.previewurl = previewurl;
    }

    public String getWebformaturl() {
        return webformaturl;
    }

    public void setWebformaturl(String webformaturl) {
        this.webformaturl = webformaturl;
    }

    public int getImagewidth() {
        return imagewidth;
    }

    public void setImagewidth(int imagewidth) {
        this.imagewidth = imagewidth;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserimageurl() {
        return userimageurl;
    }

    public void setUserimageurl(String userimageurl) {
        this.userimageurl = userimageurl;
    }

    public int getImageheight() {
        return imageheight;
    }

    public void setImageheight(int imageheight) {
        this.imageheight = imageheight;
    }
}
