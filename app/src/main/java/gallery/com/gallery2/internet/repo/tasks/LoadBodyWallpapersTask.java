package gallery.com.gallery2.internet.repo.tasks;


import android.os.AsyncTask;

import com.google.gson.Gson;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import gallery.com.gallery2.internet.repo.models.Hits;
import gallery.com.gallery2.internet.repo.models.Response;

public class LoadBodyWallpapersTask extends AsyncTask<Integer, Void, Response> {

    private String BaseUrl = "https://pixabay.com/api/";
    private String key = "?key=4514185-236aae40cd91e5d8597a4b0e4";
    private String pageParament = "&page=";
    private int Timeout = 3000;
    private static int page = 1;
    private DownloadListener downloadListener;

    public LoadBodyWallpapersTask(DownloadListener downloadListener){
        this.downloadListener = downloadListener;
    }

    @Override
    protected Response doInBackground(Integer... integers) {
        URL url = null;
        try {
            url = new URL(BaseUrl+key+pageParament+page);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        HttpURLConnection httpURLConnection = null;

        try {
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setReadTimeout(Timeout);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Response resp = null;

        try {
            BufferedInputStream inputStream = new BufferedInputStream(httpURLConnection.getInputStream());
            String response = convertToResp(inputStream);
            Gson gson = new Gson();
            resp = gson.fromJson(response,Response.class);
            page++;

        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            httpURLConnection.disconnect();
        }
        return resp;
    }

    private String convertToResp(BufferedInputStream stream){
        ByteArrayOutputStream buf = new ByteArrayOutputStream();
        try {
            int result = stream.read();

            while (result!=-1){
                buf.write((byte) result);
                result = stream.read();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return buf.toString();
    }

    @Override
    protected void onPostExecute(Response response) {
        super.onPostExecute(response);
        if (response!=null) {
            downloadListener.onPostExecute(response.getHits());
        }
    }

    public interface DownloadListener{
        void onPostExecute(List<Hits> hitsList);
    }
}
