package gallery.com.gallery2.internet.repo.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;


public class Response implements Serializable{

    @SerializedName("totalHits")
    private int totalhits;
    @SerializedName("hits")
    private List<Hits> hits;
    @SerializedName("total")
    private int total;

    public int getTotalhits() {
        return totalhits;
    }

    public void setTotalhits(int totalhits) {
        this.totalhits = totalhits;
    }

    public List<Hits> getHits() {
        return hits;
    }

    public void setHits(List<Hits> hits) {
        this.hits = hits;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
