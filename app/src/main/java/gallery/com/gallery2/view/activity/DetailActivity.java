package gallery.com.gallery2.view.activity;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;

import java.util.List;
import gallery.com.gallery2.R;
import gallery.com.gallery2.adapter.ImagePageAdapter;
import gallery.com.gallery2.internet.repo.models.Hits;

public class DetailActivity extends FragmentActivity{

    private List<Hits> list;
    private ViewPager viewPager;

    public static Intent getIntent(Context context, Hits hit){
        Intent intent = new Intent();
        return intent;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_activity);

        list = MainActivity.list;
        Intent intent = getIntent();

        viewPager = (ViewPager) findViewById(R.id.view_pager_detail_window);
        viewPager.setAdapter(new ImagePageAdapter(getSupportFragmentManager(),list));

    }
}
