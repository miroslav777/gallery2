package gallery.com.gallery2.view.interfaces;


import java.util.List;

import gallery.com.gallery2.internet.repo.models.Hits;

public interface MainWindow {
    void notifyAboutNewUrlList(List<Hits> urls);
    void showLoading();
    void showErrorMessage();
    void notifyAboutEmptyList();
    void hideLoading();
    void hideErrorMessage();
    void startDetailWindow(int position);
}

