package gallery.com.gallery2.view.fragment;


import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import gallery.com.gallery2.R;
import gallery.com.gallery2.internet.repo.models.Hits;
import gallery.com.gallery2.presenter.DetailWindowPresenter;
import gallery.com.gallery2.view.interfaces.DetailWindow;

public class DetailFragment extends Fragment implements DetailWindow{
    public static final String TAG = "DetailFragment";

    static final String ARGUMENT_PAGE_NUMBER = "arg_page_number";
    private ImageView imageView;
    private ProgressBar progressBar;
    private Hits hit;

    public static DetailFragment getFragment(Hits hit){
       DetailFragment detailFragment = new DetailFragment();
       Bundle arguments = new Bundle();
       arguments.putSerializable(ARGUMENT_PAGE_NUMBER, hit);
       detailFragment.setArguments(arguments);
       return detailFragment;
   }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.detail_item_image,container,false);
        imageView = (ImageView) view.findViewById(R.id.detail_image_cell);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar_image_cell);
        hit = (Hits) getArguments().getSerializable(ARGUMENT_PAGE_NUMBER);

        DetailWindowPresenter.getInstance(this, hit);
        return view;
    }


    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void hidePicture() {
        imageView.setVisibility(View.GONE);
    }

    @Override
    public void showPicture(Bitmap bitmap) {

        imageView.setImageBitmap(bitmap);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setVisibility(View.VISIBLE);
    }
}
