package gallery.com.gallery2.view.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;

import java.util.ArrayList;
import java.util.List;

import gallery.com.gallery2.R;
import gallery.com.gallery2.adapter.MainRecyclerViewAdapter;
import gallery.com.gallery2.internet.repo.models.Hits;
import gallery.com.gallery2.presenter.MainPresenter;
import gallery.com.gallery2.view.interfaces.MainWindow;

public class MainActivity extends Activity implements MainWindow {

    private MainPresenter mainPresenter;
    private RecyclerView recyclerView;
    private MainRecyclerViewAdapter recyclerViewAdapter;
    private GridLayoutManager gridLayoutManager;
    public static List<Hits> list;
    int columnNo;
    private int requestCode = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        columnNo = calculateNoOfColumns(this);

        gridLayoutManager = new GridLayoutManager(MainActivity.this, columnNo);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return position == list.size() ? columnNo : 1;
            }
        });

        list = new ArrayList<>();


        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleImetCount = gridLayoutManager.getChildCount();
                int totalItemCount = gridLayoutManager.getItemCount();
                int firstVisibleItems = gridLayoutManager.findFirstVisibleItemPosition();
                if ((visibleImetCount+firstVisibleItems) >= totalItemCount ){
                    mainPresenter.notifyAboutEmptyList();
                }
            }
        });

        mainPresenter = new MainPresenter(this);
        recyclerViewAdapter = new MainRecyclerViewAdapter(list,this,mainPresenter);
        recyclerView.setAdapter(recyclerViewAdapter);
    }

    @Override
    public void notifyAboutNewUrlList(List<Hits> urls) {
        list.addAll(urls);
        recyclerViewAdapter.notifyItemRangeInserted(list.size()-urls.size(), urls.size());
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void showErrorMessage() {

    }

    @Override
    public void notifyAboutEmptyList() {
        mainPresenter.notifyAboutEmptyList();
    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void hideErrorMessage() {

    }

    @Override
    public void startDetailWindow(int position) {
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra("position", position);
        startActivityForResult(intent,requestCode);
    }


    public int calculateNoOfColumns(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        int noOfColumns = (int) (dpWidth / 145);
        return noOfColumns;
    }
}
