package gallery.com.gallery2.view.interfaces;


import android.graphics.Bitmap;

public interface DetailWindow {
    void showLoading();
    void hideLoading();
    void hidePicture();
    void showPicture(Bitmap bitmap);
}
