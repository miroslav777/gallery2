package gallery.com.gallery2.view.interfaces;


import android.graphics.Bitmap;

public interface ViewCell {
    void showPicture(Bitmap picture);
    void hidePicture();
    void showLoading();
    void hideLoading();
    void setProgress(int progress);
    void hideProgress();
}
