package gallery.com.gallery2.model;

import android.graphics.Bitmap;

/**
 * Created by mshipilov on 20/02/17.
 */

public class Picture {

    private Bitmap bitmap;
    private String url;

    public Picture(Bitmap bitmap, String url) {
        this.bitmap = bitmap;
        this.url = url;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
