package gallery.com.gallery2.util;


import java.util.ArrayList;
import java.util.List;

public class TaskManager {

    private List<String> urls;

    public TaskManager(){
        urls = new ArrayList<>();
    }

    public void addUrl(String url){
        urls.add(url);
    }

    public boolean isExist(String url){

        for (String str : urls){
            if (str.equals(url)){
                return true;
            }
        }
        return false;
    }
}
