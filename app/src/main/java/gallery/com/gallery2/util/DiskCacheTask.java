package gallery.com.gallery2.util;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import gallery.com.gallery2.internet.repo.tasks.LoadImageListTask;
import gallery.com.gallery2.model.Picture;

public class DiskCacheTask  {

    private Context context;
    private static final String DISK_CACHE_SUBDIR = "wallpapers";
    private File cacheDir ;


    public DiskCacheTask(Context context){
        this.context = context;
        cacheDir = getDiskCacheDir(context,DISK_CACHE_SUBDIR);
        cacheDir.mkdirs();
    }


    private File getDiskCacheDir(Context context, String uniqueName) {

        final String cachePath = Environment.getExternalStorageDirectory().getPath();
              /*  Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState()) ||
                        !isExternalStorageRemovable() ? context.getExternalCacheDir().getPath() :
                        context.getCacheDir().getPath();*/

        return new File(cachePath + File.separator + uniqueName);
    }

    private String replaceSlashAndSubstring(String str){
        String[] splitString = str.split(":");
        String newString = splitString[1].replace("/", "_");
        return newString;
    }


    public void loadImageToDir(Picture picture){
        new LoadImageToDir().execute(picture);
    }

    public AsyncTask loadImageFromDir(LoadImageListTask.DownloadListener downloadListener, String url){
       return new LoadImageFromDir(downloadListener).execute(url);
    }



    class LoadImageToDir extends AsyncTask<Picture,Void,Void>{

        @Override
        protected Void doInBackground(Picture... pictures) {
            String name = replaceSlashAndSubstring(pictures[0].getUrl());
            File image = new File (cacheDir, name);
            FileOutputStream outStream;
            try {
                outStream = new FileOutputStream(image);
                pictures[0].getBitmap().compress(Bitmap.CompressFormat.JPEG, 100, outStream);
                outStream.flush();
                outStream.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    class LoadImageFromDir extends AsyncTask<String,Void,Bitmap>{

        private LoadImageListTask.DownloadListener downloadListener;

        public LoadImageFromDir(LoadImageListTask.DownloadListener downloadListener){
            this.downloadListener = downloadListener;
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            String fileName = replaceSlashAndSubstring(params[0]);
            File file = new File (cacheDir,fileName);
            Bitmap bitmap = null;

            try {
                bitmap = BitmapFactory.decodeStream(new FileInputStream(file));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            downloadListener.onPostExecute(bitmap);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            if (downloadListener!=null){
                downloadListener = null;
            }
        }
    }
}
