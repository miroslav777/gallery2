package gallery.com.gallery2.adapter;


import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import java.util.List;
import gallery.com.gallery2.R;
import gallery.com.gallery2.internet.repo.models.Hits;
import gallery.com.gallery2.presenter.ViewCellPresenter;
import gallery.com.gallery2.view.interfaces.ViewCell;

public class MainRecyclerViewAdapter  extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private List<Hits> urls;
    private LayoutInflater layoutInflater;
    private static Listener listener;

    public interface Listener{
        void onClick(int position);
    }

    public MainRecyclerViewAdapter(List<Hits> urls, Context context, Listener listener){
        this.urls = urls;
        this.layoutInflater = LayoutInflater.from(context);
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        switch (viewType){
            case 0: view = layoutInflater.inflate(R.layout.image_cell,parent,false);
                break;
            case 1: view = layoutInflater.inflate(R.layout.upload_progress_cell,parent,false);
                break;
        }
        return new ImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (urls.size()!=0 &&  position < urls.size()){
            ((ImageViewHolder) holder).setHits(urls.get(position), position);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position == urls.size() ? 1 : 0;
    }

    @Override
    public int getItemCount() {
        return urls.size()+1;
    }

    public static class ImageViewHolder extends RecyclerView.ViewHolder implements ViewCell{

        private ImageView imageView;
        private ViewCellPresenter viewCellPresenter;
        private ProgressBar progressBar;
        private int position;

        public ImageViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.imageViewCell);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressbar);
            viewCellPresenter = new ViewCellPresenter(this);
            if (imageView!=null) {
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (listener == null){
                            return;
                        }
                        listener.onClick(position);
                    }
                });
            }
        }

        public void setHits(Hits hits, int position) {
            this.position = position;
            viewCellPresenter.uploadPicture(hits.getPreviewurl());
        }

        @Override
        public void showPicture(Bitmap picture) {
            imageView.setVisibility(View.VISIBLE);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setImageBitmap(picture);
        }

        @Override
        public void hidePicture() {
            imageView.setVisibility(View.GONE);
        }


        @Override
        public void showLoading() {
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        public void hideLoading() {
            progressBar.setVisibility(View.GONE);
        }

        @Override
        public void setProgress(int progress) {

        }

        @Override
        public void hideProgress() {
        }
    }

}
