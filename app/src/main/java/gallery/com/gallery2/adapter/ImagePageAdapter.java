package gallery.com.gallery2.adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import java.util.List;
import gallery.com.gallery2.internet.repo.models.Hits;
import gallery.com.gallery2.view.fragment.DetailFragment;

public class ImagePageAdapter extends FragmentPagerAdapter {

    private List<Hits> list;

    public ImagePageAdapter(FragmentManager fm, List<Hits> list) {
        super(fm);
        this.list = list;
    }

    @Override
    public Fragment getItem(int position) {
        return DetailFragment.getFragment(list.get(position));
    }

    @Override
    public int getCount() {
        return list.size();
    }
}
