package gallery.com.gallery2;


import android.app.Application;

import gallery.com.gallery2.internet.repo.LoadImageRepository;
import gallery.com.gallery2.internet.repo.WallpapersAPIRepo;
import gallery.com.gallery2.util.DiskCacheTask;
import gallery.com.gallery2.util.TaskManager;

public class MyApp extends Application{


    private static WallpapersAPIRepo wallpapersAPIRepository;
    private static LoadImageRepository loadImageRepository;
    private static TaskManager taskManager;
    private static DiskCacheTask diskCacheTask;

    public MyApp(){
        loadImageRepository = new LoadImageRepository();
        wallpapersAPIRepository = new WallpapersAPIRepo();
        taskManager = new TaskManager();
        diskCacheTask = new DiskCacheTask(getBaseContext());
    }


    public static WallpapersAPIRepo getWallpapersAPIRepository(){
        return wallpapersAPIRepository;
    }

    public static LoadImageRepository getLoadImageRepository(){
        return loadImageRepository;
    }

    public static TaskManager getTaskManager(){
        return taskManager;
    }

    public static DiskCacheTask getDiskCacheTask(){
        return diskCacheTask;
    }
}
